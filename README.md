search-replace-utils
====================

Python scripts to search and replace entire files and strings within files

The script "SearchReplaceFiles" will prompt you for a filename, the string
to be replaced, and the replacement string.  It will then recursively search
the current CWD and make replacements in all files found with the specified
filename.  Replacement is limited to a string contained within a single line.

The script "SearchReplaceEntireFiles" will prompt you for the name of a
file in the current CWD.  It will then recursively search all subdirectories
inside the CWD for files with the same name, and all such files will be
replaced with the corresponding file in the CWD.
